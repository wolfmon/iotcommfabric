#!/usr/bin/env python
'''
app_consumer.py 
Mock Data Consumer App to demonstrate Fabric Functions.

This application will:
1.Connect to DBus System Bus
2.Attach to signal and bind signal hander that indicates incoming data is
available in the Fabric Daemon's (NON-BLOCKING!) Queue.
3.Retrieve the data from the queue
4.Display the data tot STDOUT.

'''
import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GObject as gobject

import threading
import thread
import time

#Blocks retiever method when no data is in the Daemon's Queue
sem = threading.Semaphore(0)

#DBusMain Event Loop
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

#DBus Service Object
bus = dbus.SystemBus()
service = bus.get_object('net.wolfmon.do.fabric', "/net/wolfmon/do/fabric")
#DBus Method Object(Remote/Callable)
get_method = service.get_dbus_method('get','net.wolfmon.do.fabric.FabricOps')

'''
Callback function to handle singnal that indicates data is available in the 
daemon Q. Increase semaphore by the available elements in the daemon queue.
'''
def fabric_has_data_signal_handler(qsize):
	print("[APP]Got Signal, there are "+ str(qsize) + " elements in fabric Queue")
	for x in range(int(qsize)):
		sem.release()

'''
Function to retrieve the data using fabric interface, block on semaphore empty, which
indicates no data is available in the Fabric daemon's Queue.
'''
def get_data_from_fabric():
	while True:
			time.sleep(0.01)
			sem.acquire()
			rtn = get_method() 
			try:
					if (rtn == ''):
						pass
					else:
						print ("[APP]Remote Data: " + str(rtn))
			except:
				pass

#Connect to signal
service.connect_to_signal("fabric_has_data_signal", fabric_has_data_signal_handler, dbus_interface="net.wolfmon.do.fabric.FabricOps.QHasData")

#Two event loops, no ungarded shared resources.
thread.start_new_thread(get_data_from_fabric,())
loop = gobject.MainLoop()
loop.run()
