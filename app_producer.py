#!/usr/bin/env python
'''
app_producer.py
This applilcation will:
1.Attach to DBus System Bus.
2.Generate Randmo IoT data
3.Invoke DBus Interface method to send the data to destnation.
'''
import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GObject as gobject

import random
import time


#destination Address
server_address = "10.5.7.240"

#DBus Service Object
bus = dbus.SystemBus()
service = bus.get_object('net.wolfmon.do.fabric', "/net/wolfmon/do/fabric")
#DBus method Object (remote/callable)
send_method = service.get_dbus_method('send','net.wolfmon.do.fabric.FabricOps')

#seed
random.seed()

#main loop
while True:
	sensor_val = random.randint(1,100)
	send_method(str(sensor_val), server_address)
	print("[APP]Sending " + str(sensor_val))
	#delay 
	time.sleep(2)
