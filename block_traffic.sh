#!/bin/sh
#This script blocks bidirectional traffic by setting netfilter, example usage: ./block_traffic.sh <destination_IP>
BLOCK_THIS_IP=$1
iptables -A OUTPUT -j DROP -d "$BLOCK_THIS_IP"
iptables -A INPUT -s "$BLOCK_THIS_IP" -j DROP
