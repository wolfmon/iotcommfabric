#!/usr/bin/env python

'''
This component is responsible for handing local communcation to the Fabric Service.
This components exposes fabric functions to be called over DBus.
This component will:
1. Register Service with DBus System Bus
2. Expose Fabric  API Methods
3. Route Remote Method calls to approiate callback functions.
4. Parse request into Protobuf.
5. Send Serialized Request packet into message queue.
6. Countinuesly monitor message on incoming message Queue.
7. Deserialize incomming packet and route to apporiate handler.
8. If application data is present, enque the data and signal the client.

@param
msg_q_tx : TX Queue handle
msg_q_rx : RX queue handle
my_address: local address
friendly_name: human-readable name for identifying the node.
'''
#System utilities
import struct
import socket
import time
import sys
import thread
from Queue import Queue

import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GObject as gobject

import fabric_pb2 as fab
from google.protobuf.any_pb2 import Any



"""
DBus Reference
https://gist.github.com/caspian311/4676061

Signal Example:
https://github.com/zyga/dbus-python/blob/master/examples/example-signal-emitter.py
"""

#Inherting dbus.service.Object
class DBusInterface(dbus.service.Object):

	def __init__ (self, msg_q_tx, msg_q_rx, my_address, friendly_name):
		print("[DBUS]Starting service...")
		dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
		bus_name = dbus.service.BusName("net.wolfmon.do.fabric", dbus.SystemBus())
		dbus.service.Object.__init__(self, bus_name, "/net/wolfmon/do/fabric")
		self._msg_q_tx = msg_q_tx
		self._msg_q_rx = msg_q_rx
		self._my_address = my_address	
		self._friendly_name = friendly_name	
		self._fabric_state = "Offline"
		self._data_q = Queue(maxsize=0)
		self._dbus_msg_handler = {
							"app_data":self.on_app_data_recv
							#"register":self.send_register,
							#"updateList":self.handle_updateList,
							#"app_msg":self.send_app_msg
							}

	'''
	The following function validate dbus function, not used anymore.
	functional at commit id:2266d2484404a7fbe928c25ddc9035ea8789b370
	
	'''
	@dbus.service.method("net.wolfmon.do.fabric.Hello", in_signature='i', out_signature='d')
	def Hello(self, num):
		print ("Ohla!! I got", num)
		return 1
	'''
	The following function validate dbus function, not used anymore.
	functional at commit id:2266d2484404a7fbe928c25ddc9035ea8789b370
	'''
	@dbus.service.method("net.wolfmon.do.fabric.SelfTest", in_signature='i', out_signature='i')
	def Selftest(self, num):
		print ("Selftest Invoked", num)

		message = fab.FabricMessage()
		# convert IP address from string to packed bytes representation
		# message.appData.source.address = struct.unpack('!L',socket.inet_aton("127.0.0.1"))[0]
		# message.appData.destnation.address = struct.unpack('!L',socket.inet_aton("127.0.0.1"))[0]
		#message.appData.source.name = "Test"
		#message.appData.destnation.name = "Test"
		return num + 1
	'''
	The following function validate dbus function, not used anymore.
	functional at commit id:2266d2484404a7fbe928c25ddc9035ea8789b370
	'''
	@dbus.service.method("net.wolfmon.do.fabric.FabricStatus", in_signature='', out_signature='s')
	def getStatus(self):
		return self._fabric_state
	'''
	The following function validate dbus function, not used anymore.
	This function invokes a inter-daemon communication to query ZMQ status.
	functional at commit id:2266d2484404a7fbe928c25ddc9035ea8789b370
	'''
	@dbus.service.method("net.wolfmon.do.fabric.FabricStatus", in_signature='', out_signature='s')
	def updateStatus(self):
		print ("[DBUS]Querying for Fabric Status...")
		message = fab.IPCMessage()
		message.request = "getStatus"
		self._msg_q_tx.put(message.SerializeToString())
		return "OK"
	'''
	Remote API for client apps to get data
	'''
	@dbus.service.method("net.wolfmon.do.fabric.FabricOps", in_signature='', out_signature='s')
	def get(self):
		if(self._data_q.empty()):
			return ''
		else:
			message = self._data_q.get()
			q_any = Any()
			q_any.CopyFrom(message.app_data.payload[0])
			unpacked_msg = fab.AppPayload()
			q_any.Unpack(unpacked_msg)
			print ("[DBUS GET] Passing payload to App:", unpacked_msg)
			return unpacked_msg.str_payload
	
	'''
	Remote API for client apps to send data
	example usage: send ("somedata", "128.230.200.200")
	'''
	
	@dbus.service.method("net.wolfmon.do.fabric.FabricOps", in_signature='ss', out_signature='s')
	def send(self,data, d_address):
		print ("[DBUS SEND]Sending Data:" + str(data) +" to "+ str(d_address))
		#To do, validate data and address
		fb_message = fab.FabricMessage()
		fb_message.destnation.address = d_address;
		fb_message.source.address = self._my_address
		fb_message.source.name = self._friendly_name
		fb_payload = fab.AppPayload()
		fb_payload.str_payload = data;
		pb_any = Any()
		pb_any.Pack(fb_payload)
		fb_message.app_data.payload.extend([pb_any])
		self._msg_q_tx.put(fb_message.SerializeToString())
		
		return "ok"

	'''
	Method to raise signal indication incoming data is ready for client.
	'''

	@dbus.service.signal("net.wolfmon.do.fabric.FabricOps.QHasData", signature='i')
	def fabric_has_data_signal(self, elems_in_q):
		pass

	#event loop
	def run_dbus_event_loop(self):
		try:
			self._loop.run()
		except KeyboardInterrupt:
			self._loop.quit()
	'''
	The following function validate dbus function, not used anymore.
	functional at commit id:2266d2484404a7fbe928c25ddc9035ea8789b370
	'''
	def dbus_selftest(self):
		print("[DBUS]dbus self test invoked")
		bus = dbus.SystemBus()
		service = bus.get_object('net.wolfmon.do.fabric', "/net/wolfmon/do/fabric")
		selftest_method = service.get_dbus_method('Selftest','net.wolfmon.do.fabric.SelfTest')
		rtn = selftest_method(91)
		print ("return :", rtn)
		# dbus_proxy_object = dbus.SessionBus().get_object("net.wolfmon.do.fabric", "/net/wolfmon/do/fabric")
		# print (dbus_proxy_object.Selftest(dbus_interface= 'net.wolfmon.do.fabric'))

	'''
	Recieve Q monitor. Inter-daemon not functional any more.
	To test funcation of the Inter-daemon, 
	revert to commit ID: 2266d2484404a7fbe928c25ddc9035ea8789b370

	'''
	def dbus_recv_q_monitor(self):
		while True:
			if(self._msg_q_rx.empty()):
				#print("[DBUS]RX Queue Empty, Sleeping ....")
				time.sleep(0.01)
			else:
				#print("[DBUS RECV QUEUE]Q Recieved Message\n")
				q_raw_message = self._msg_q_rx.get()
				q_message = fab.FabricMessage.FromString(q_raw_message)
				handler = self._dbus_msg_handler[q_message.WhichOneof("payload")]
				handler(q_message)

				# q_message = fab.IPCMessage.FromString(q_raw_message)
				# q_any = Any()
				# q_any.CopyFrom(q_message.data[0])
				# unpacked_msg = fab.StatusRespond()
				# q_any.Unpack(unpacked_msg)
				# print (q_message)
				# print ("Message Type:", q_message.respond)
				# print ("Message Content:", unpacked_msg.str_respond)
				# self._fabric_state = unpacked_msg.str_respond

	'''
	Function handler to handler messag from lower ZMQ which may contains client app data.
	'''
	def on_app_data_recv(self, message):
		#print("[DBUS] Got new appdata message", message)
		self._data_q.put(message)

	'''
	Q monitor
	'''
	def data_q_monitor(self):
		data_size = 0
		while True:
			if((self._data_q.empty()) or (data_size	== self._data_q.qsize())):
				time.sleep(0.01)
			else:
				data_size = self._data_q.qsize()
				print("[DBUS]Data Q has", self._data_q.qsize(),"message, notifying user App")
				self.fabric_has_data_signal(self._data_q.qsize())
				

	'''
	Entry Point of the component.
	'''
	def run(self):
		self._loop = gobject.MainLoop()
		thread.start_new_thread(self.run_dbus_event_loop,())
		thread.start_new_thread(self.dbus_recv_q_monitor,())
		thread.start_new_thread(self.data_q_monitor,())
		#thread.start_new_thread(self.dbus_selftest,())
		#self.dbus_selftest()
		print("[DBUS] DBUS Init. completed Successfully\n")

	'''
	On Keyboard Interrupt Ctrl-C
	'''
	def stop(self):
		print ("D-Bus interface shutting down...")
		self._loop.quit()