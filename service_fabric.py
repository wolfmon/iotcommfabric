#!/usr/bin/env python

'''
This applicaiton is the main fabric service component.
This application will:
1.Connnect to each end(DBUS and ZMQ) of the communcation interface
2. Preform Initilization
3. Discover reachability maintain connectivity table to other nodes
4. Route applicaiton traffic based on reachability table.
5. Handle incoming and outgoing traffic and make internal routing decision using
(NON-Blocking) message queues (signal thread)
'''
import struct
import socket
import time
import sys
import thread
from Queue import Queue


import fabric_pb2 as fab
from google.protobuf.any_pb2 import Any

import zmq_fabric as remote_if
import dbus_fabric as local_if


from expiringdict import ExpiringDict
#/usr/local/lib/python2.7/dist-packages/expiringdict
'''
Fabric Service Class
Implments the fabric daemon.

DBUS interface with the Client Application via System Bus

							|
							|
							|
					  Queue(x4) 
							|
							|
							|

ZMQ interface with remote fabric service instance (another node)

uuid:not used as of right now
'''

class FabricService():

	def __init__(self, uuid, friendly_name, zmq_port, server_address):
		self._node_uuid = uuid
		self._friendly_name = friendly_name
		self._server_address = server_address #Own Address
		self._port = zmq_port

		self._hdlr_msg_from_zmq = {
							"register":self.handle_register,
							"updateList":self.handle_updateList,
							"keepAlive":self.handle_keepAlive,
							"app_data":self.handle_appData
							}

		#expiring dictionary to keey track of reachability
		self._active_peers = ExpiringDict(max_len=200, max_age_seconds=15)
		#global table to keep track of all nodes seen (not scaleable)
		self._all_peers = {"10.5.1.1":"Gateway","10.3.1.1":"Gateway","10.1.1.1":"Gateway"}
		self._msg_q_dbus_to_fabric_service = Queue(maxsize=0)
		self._msg_q_fabric_service_to_dbus = Queue(maxsize=0)

		self._msg_q_zmq_to_fabric_service = Queue(maxsize=0)
		self._msg_q_fabric_service_to_zmq = Queue(maxsize=0)

		#Fabric connection
		self._local_interconnect = local_if.DBusInterface(self._msg_q_dbus_to_fabric_service, self._msg_q_fabric_service_to_dbus, self._server_address, self._friendly_name	)
		self._remote_interconnect = remote_if.ZMQInterface(self._server_address,self._port,self._friendly_name ,self._msg_q_zmq_to_fabric_service, self._msg_q_fabric_service_to_zmq)

	'''
	"Daemon" to handle data flow between message queues
	'''
	def dbus_to_zmq_queue_daemon(self):
		#To ZMQ
		while True:
			if (self._msg_q_dbus_to_fabric_service.empty()):
				time.sleep(0.1)
			else:
				r_msg = self._msg_q_dbus_to_fabric_service.get()
				msg = fab.FabricMessage.FromString(r_msg)
				#Only App Data need routing, the rest can go directly
				if((msg.WhichOneof("payload") == "app_data")):
					self.route_traffic(msg)
				else:
					self._msg_q_fabric_service_to_zmq.put(r_msg)

	'''
	"Daemon" to handle data flow between message queues
	'''
	def zmq_to_dbus_queue_daemon(self):
		#To DBus
		while True:
			if (self._msg_q_zmq_to_fabric_service.empty()):
				time.sleep(0.1)
			else:
				raw_message = self._msg_q_zmq_to_fabric_service.get()
				#print ("Fabric Service Getting message:", raw_message)
				message = fab.FabricMessage.FromString(raw_message)
				#print ("Type:", message.WhichOneof("payload"))
				
				handler = self._hdlr_msg_from_zmq[message.WhichOneof("payload")]
				handler(message)

				

				#self._msg_q_fabric_service_to_dbus.put()

	'''
	Send Registration message, valid node will respond with their node table.
	'''
	def send_register(self, destnation):
		message = fab.FabricMessage()
		message.register.d_addr = destnation
		#self._active_peers[destnation] = "Initial"
		self._all_peers[destnation] = "Initial"
		message.register.nodeinfo.address = self._server_address
		message.register.nodeinfo.name = self._friendly_name
		print("[FABRIC SRV]Initiating register message...")
		self._msg_q_fabric_service_to_zmq.put(message.SerializeToString())

	'''
	Send current node list to another peer node. Sync node list.
	'''
	def send_updateList(self, destnation):
		message = fab.FabricMessage	()
		message.updateList.d_addr	= destnation
		for key, value in self._all_peers.items():
			  if key == destnation:
			  	pass
			  else:
						ni = fab.NodeInfo()
						ni.address = key
						ni.name = value
						message.updateList.nodelist.nodeinfo.extend([ni])
		print("[FABRIC SRV]Initiating Update message...")
		self._msg_q_fabric_service_to_zmq.put(message.SerializeToString())

	'''
	Keep Alive packet, will keep node valid in reachability table/dict.
	'''
	def send_keepAlive(self, destnation):
		message = fab.FabricMessage()
		message.keepAlive.d_addr = destnation
		message.keepAlive.nodeinfo.address = self._server_address
		message.keepAlive.nodeinfo.name = self._friendly_name
		print("[FABRIC SRV]Sending KeepAlive to " + str(destnation))
		self._msg_q_fabric_service_to_zmq.put(message.SerializeToString())

	'''
	Send keep alive periodically
	'''
	def keepAliveDaemon(self):
		while True:
			time.sleep(10)
			for keys in self._all_peers.keys():
				self.send_keepAlive(keys)

	'''
	Registration handler
	Handle registration message from remote node.
	Register remote node.
	Send reply with list update.
	'''
	def handle_register(self,message):
		
		remote_addr = message.register.nodeinfo.address
		remote_name = message.register.nodeinfo.name
		self._active_peers [remote_addr] = remote_name
		self._all_peers [remote_addr] = remote_name
		print("[FABRIC SERVICE]Registering " + str(remote_name	) + " at " + str(remote_addr))
		self.send_updateList(remote_addr)

	'''
	Handler incoming list sync messag from remote node.
	'''
	def handle_updateList(self,message):

			if len(message.updateList.nodelist.nodeinfo) == 0:
				pass
			else:
				#print("handling updateList", message)
				for i in message.updateList.nodelist.nodeinfo:
					print("Adding "+ str(i.name) + " at " + str(i.address))
					self._all_peers [i.address] = i.name
					self._active_peers[i.address] = i.name

	'''
	Keep entry alive in reachability data.
	'''
	def handle_keepAlive(self, message):
		remote_addr = message.keepAlive.nodeinfo.address
		remote_name = message.keepAlive.nodeinfo.name
		self._active_peers [remote_addr] = remote_name
		print("[FABRIC SERVICE]Keeping Alive: " + str(remote_name) + " at " + str(remote_addr))

	'''
	Handle app data. Forward to upper layer if destination match this node's destination.
	Otherwise fowrward back down for routing decision.
	'''
	def handle_appData(self, message):
		if (message.destnation.address == self._server_address):
			self._msg_q_fabric_service_to_dbus.put(message.SerializeToString())
		elif (message.ttl == 0):
			pass
		else:
			message.ttl = message.ttl - 1
			self.route_traffic(message)
	'''
	Sync list accross fabric (not scaleable)
	'''
	def sync_list(self):
		for address in self._active_peers.keys():
			self.send_updateList(address)
		time.sleep(45)
	'''
	Route traffic based on reachability table.
	If remote node specified in destination address is reachable.
	next hop to that address.
	If not, grab another node in the list and set next hop to that node.
	if no node available to take traffic, drop it.
	'''
	def route_traffic(self,message):
		msg_to_zmq = message
		for key, value in self._active_peers.items():
			print (key, value)
		if(self._active_peers.get(message.destnation.address,"DNE") == "DNE"):
			print("[FABRIC SRV] " + str(message.destnation.address) + " is not reachable, attempting routing... " + str(len(self._active_peers)) )
			if(len(self._active_peers) == 0):
				print("[FABRIC SRV] Traffic Not Routable, Dropped")
				pass
			else:
				next_hop_addr = self._active_peers.keys()[-1]
				msg_to_zmq.nexthop.address = next_hop_addr
				msg_to_zmq.ttl = 2
				print("[FABRIC SRV] Routing Traffic via: " +str(next_hop_addr))
				self._msg_q_fabric_service_to_zmq.put(msg_to_zmq.SerializeToString())
		else:
			print("[FABRIC SRV] " + "Destnation reachable, sending traffic directly...")
			msg_to_zmq.nexthop.address = msg_to_zmq.destnation.address
			self._msg_q_fabric_service_to_zmq.put(msg_to_zmq.SerializeToString())











	def run(self):

		thread.start_new_thread(self.dbus_to_zmq_queue_daemon,())

		thread.start_new_thread(self.zmq_to_dbus_queue_daemon,())

		thread.start_new_thread(self.keepAliveDaemon,())

		thread.start_new_thread(self.sync_list,())
		self._local_interconnect.run()
		self._remote_interconnect.run()
		time.sleep(0.5)
		print("[FABRIC SERVICE] Init. Successful")
		self.send_register("10.5.7.240")
		while True:
			time.sleep(100)
	def stop(self):
		self._local_interconnect.stop()
		self._remote_interconnect.stop()

if __name__ == "__main__":
	service = FabricService(1234,"IoT_Node_239", 8080,"10.5.7.239")
	try:
		service.run()
	except KeyboardInterrupt:
		print("Interrrupt recieved, stopping fabric service...")
		service.stop()


