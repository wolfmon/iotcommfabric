#!/usr/bin/env python
'''
This component is the lower layer interface for fabric service. 
Is responsible for putting serialized traffic on to the "wire"
Is represented as the PHY for fabric.
This packet depends on protobuf defination of the fabric.
This component will:
1. initialilze ZMQ push-pull socket
2. start listen for imcoming message on socket and message queue.
3. once the socket is recieved, forward to upper layer.
4. once request is recieved in message queue. Determin message type and forward accordingly.
5. If message requires transmission, send over socket.
6. If message is state query, reply upper layer via message queue.
'''
import struct
import socket
import time
import sys
import thread
from Queue import Queue

#Fabric Tranciever
import zmq

import fabric_pb2 as fab
from google.protobuf.any_pb2 import Any


class ZMQInterface():

	def __init__(self,server_address, zmq_port, friendly_name, msg_q_tx, msg_q_rx):
		self._msg_q_tx = msg_q_tx
		self._msg_q_rx = msg_q_rx
		self._reg_server_address = server_address
		self._friendly_name = friendly_name	
		self._port = zmq_port
		self._zmq_socket_status = "Offline"
		self._data_q = Queue(maxsize=0)
		self._z_context = zmq.Context()


		self._zmq_socket_status = "Online"
		print("[ZMQ]Initializing with " + str(self._reg_server_address)+ ":"+str(self._port))


		self.fb_msg_handler = {
							"ipc_msg":self.handle_ipc_msg,
							"register":self.send_register,
							"updateList":self.send_updateList,
							"app_data":self.send_app_data,
							"keepAlive":self.send_keepAlive
							}

		self.ipc_request_handler = {

									"getStatus":self.handle_getStatus
									}

'''
Mesage Handlers for message types defined 
'''

	def handle_ipc_msg(self, message):
		print("[ZMQ]IPC handler called", message)
		if (message.ipc_msg.WhichOneof("command") == "request"):
			print("handling request")
			handler = self.ipc_request_handler[message.ipc_msg.request]
			handler()
		else:
			print("got response")




	def send_register(self, message):
		print("[ZMQ]sending register message:"+ '\n\n'+ str(message))
		sender = self._z_context.socket(zmq.PUSH)
		sender.connect("tcp://%s:%i" % (message.register.d_addr, 8080))
		sender.send(message.SerializeToString())
		sender.close()

	def send_updateList(self, message):
		print("[ZMQ]sending Update message:"+ '\n\n'+ str(message))
		sender = self._z_context.socket(zmq.PUSH)
		sender.connect("tcp://%s:%i" % (message.updateList.d_addr, 8080))
		sender.send(message.SerializeToString())
		sender.close()
	def send_keepAlive(self, message):
		print("[ZMQ]sending KeepAlive message:"+ '\n\n'+ str(message))
		sender = self._z_context.socket(zmq.PUSH)
		sender.connect("tcp://%s:%i" % (message.keepAlive.d_addr, 8080))
		sender.send(message.SerializeToString())
		sender.close()


'''
sending appdata to wire
'''
	def send_app_data(self, message):
		#print("[ZMQ]Sending AppData...")
		#print(message)
		sender = self._z_context.socket(zmq.PUSH)
		#print("----" + str( message.nexthop.address))
		sender.connect("tcp://%s:%i" % (message.nexthop.address, 8080))
		sender.send(message.SerializeToString())
		sender.close()
		print("[ZMQ]App Data Sent.")



'''
listen loop, forward directly to message queue, this compoent does not make
any decisions on incoming packets.
'''
	def zmq_listen_loop(self):
		#print("[ZMQ]Started Listening...")
		receiver = self._z_context.socket(zmq.PULL)
		receiver.bind("tcp://*:8080")
		while True:
			raw_message = receiver.recv()

			self._msg_q_tx.put(raw_message)


			

'''
Message queue data handler.
'''
	def zmq_recv_q_monitor(self):
		while True:
			if(self._msg_q_rx.empty()):
				#print("[ZMQ]RX Queue Empty, Sleeping ....")
				time.sleep(0.01)
			else:
				print("[ZMQ]Processing Message from FABRIC SERVICE\n")
				q_raw_message = self._msg_q_rx.get()
				q_message = fab.FabricMessage.FromString(q_raw_message)

				handler = self.fb_msg_handler[q_message.WhichOneof("payload")]
				handler(q_message)

'''
Query handler, not functional/used anymore since
commit id: 2266d248
'''
	def handle_getStatus(self):
		print("[ZMQ]getStatus handler invoked")
		message = fab.IPCMessage()
		message.respond = "getStatus"
		res = fab.StatusRespond()
		res.str_respond = self._zmq_socket_status
		pb_any = Any()
		pb_any.Pack(res)
		message.data.extend([pb_any])
		self._msg_q_tx.put(message.SerializeToString())



'''
Component entry point
'''
	def run(self):
		print("[ZMQ]Interface Initializing...")
		thread.start_new_thread(self.zmq_listen_loop,())
		_zmq_socket_status= "Online"
		thread.start_new_thread(self.zmq_recv_q_monitor,())
		print("[ZMQ]Interface Ready.")
		
'''
stop on keyboard interrupt ctrl-c
'''
	def stop(self):
		self.poller.unregister(self._listen_socket)
		self._listen_socket.close()
		self._context.term()